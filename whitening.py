import numpy as np


class SpectrumWhitening:
    def __init__(self, signal, sample_rate, window_length, avance_ventana):
        self.signal = signal
        self.sample_rate = sample_rate
        self.window_length = window_length
        self.avance_ventana = avance_ventana

    def num_windows(self):
        signal_length = len(self.spectral())
        num_windows = int(((signal_length - self.window_length) / self.avance_ventana * 100) + 1)
        return num_windows

    def spectral(self):
        signal_no_mean = self.signal - self.signal.mean()
        window = np.blackman(len(signal_no_mean))
        signal_windowed = signal_no_mean * window
        signal_in_frequency = np.fft.fft(signal_windowed)
        return signal_in_frequency

    def whitening_signal(self, signal_spectral):
        if (self.window_length % 2) == 0:
            window_middle_index_element = self.window_length // 2
        else:
            window_middle_index_element = self.window_length // 2

        whitened_signal = signal_spectral[0:window_middle_index_element] + signal_spectral[-window_middle_index_element:]

        for i in range(0, len(signal_spectral) - (self.window_length - 1)):
            window = signal_spectral[i:i + self.window_length]
            window_mean = np.mean(window)
            whitened_signal.insert(2 + i, window_mean)
        return whitened_signal
