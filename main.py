from whitening import SpectrumWhitening
import numpy as np
import matplotlib.pyplot as plt


if __name__ == '__main__':
    avance_ventana = 1
    window_length = 5
    sample_rate = 100.0
    frecuencia = 800.0
    amplitud = 2 * np.sqrt(2)
    signal_length = 1000
    time = np.arange(signal_length) / sample_rate
    signal = amplitud * np.sin((np.pi/180) * frecuencia * time)

    spectrum_whitening = SpectrumWhitening(signal, sample_rate, window_length, avance_ventana)

    spectral = spectrum_whitening.spectral().tolist()

    w_signal = spectrum_whitening.whitening_signal(spectral)

    fig, [ax1, ax2, ax3] = plt.subplots(3, 1, sharex=True)

    ax1.set_title('Signal')
    ax1.plot(signal)
    ax1.grid(True)

    ax2.set_title('Spectral')
    ax2.plot(spectral)
    ax2.grid(True)

    ax3.set_title('Whitened Signal')
    ax3.plot(w_signal)
    ax3.grid(True)

    plt.show()
